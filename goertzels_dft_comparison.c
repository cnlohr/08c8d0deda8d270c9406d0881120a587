#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define CNFG_IMPLEMENTATION

#include "rawdraw_sf.h"

void HandleKey( int keycode, int bDown ) { }
void HandleButton( int x, int y, int button, int bDown ) { }
void HandleMotion( int x, int y, int mask ) { }
void HandleDestroy() { }

int main()
{
	CNFGSetup( "Windowname", 1024, 768 );
	#define SAMPS 1024
	double data[SAMPS];
	int i;
	for( i = 0; i < SAMPS; i++ )
	{
		data[i] = sin( i + 1.57 );
	}
	
	CNFGBGColor = 0x000000;

	do
	{
		CNFGClearFrame();
		CNFGHandleInput();
		int k;
		for( k = 0; k < 1024; k++ )
		{
			//double kOm = 2.0*pi*k/N;
			double kOm = k / 600.0;


			// Goertzel's
			double realW = 2.0*cos(kOm);
			double imagW = sin(kOm);
			
			double d1 = 0.0;
			double d2 = 0.0;
			for( i = 0; i < 256; i++ )
			{
				double y = data[i] + realW * d1 - d2;
				d2 = d1;
				d1 = y;
			}
			double resultr = 0.5*realW*d1 - d2;
			double resulti = imagW*d1;


			// DFT
			double resultrDFT = 0;
			double resultiDFT = 0;
			for( i = 0; i < 256; i++ )
			{
				resultrDFT += cos( i * kOm ) * data[i];
				resultiDFT += sin( i * kOm ) * data[i];
			}

			double rv = sqrt( resultr * resultr + resulti * resulti );
			double rvDFT = sqrt( resultrDFT * resultrDFT + resultiDFT * resultiDFT );
			CNFGColor( 0xffff00ff );
			CNFGTackPixel( k, -resultrDFT*4+384 );
			CNFGColor( 0xff00ffff );
			CNFGTackPixel( k+1, resulti*4+384 );
		}
		CNFGSwapBuffers();
	} while( 1 );
}
